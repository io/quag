OperatorTable do(
	addOperator := method(symbol, precedence,
		precedence = precedence ifNilEval(0)
		if(precedence < 0 or precedence >= precedenceLevelCount,
			Exception raise("Precedence for operators must be between 0 and " .. levelCount - 1 .. ". Precedence was " .. precedence .. ".")
		)
		operators atPut(symbol, precedence)
		self
	)

	addAssignOperator := method(symbol, messageName,
		assignOperators atPut(symbol, messageName)
		self
	)

	addOpEqualsOperator := method(symbol,
		opEqualsOperators atPut(symbol, nil)
	)

	asString := method(
		s := Sequence clone appendSeq(OperatorTable asSimpleString, ":\n")

		s appendSeq("Operators")
		OperatorTable operators values unique sort foreach(precedence,
			s appendSeq("\n  ", precedence asString alignLeft(4), OperatorTable operators select(k, v, v == precedence) keys sort join(" "))
		)

		s appendSeq("\n\nAssignment Operators")
		OperatorTable assignOperators keys sort foreach(symbol,
			name := OperatorTable assignOperators at(symbol)
			s appendSeq("\n  ", symbol alignLeft(4), name)
		)

		s appendSeq("\n\nOp-Equals Operators")
		s appendSeq("\n  ", OperatorTable opEqualsOperators keys sort join(" "))

		s appendSeq("\n\n")
		s appendSeq("To add a new operator: OperatorTable addOperator(\"+\", 4) and implement the + message.\n")
		s appendSeq("To add a new assign operator: OperatorTable addAssignOperator(\"=\", \"updateSlot\") and implement the updateSlot message.\n")
		s appendSeq("To add a new op-equals operator: OperatorTable addOpEqualsOperator(\"+=\") and implement the += message.\n")

		s
	)

	reverseAssignOperators := method(assignOperators reverseMap)
)

# Make the lookup path shorter for the opShuffle. IoMessage_opShuffle looks up
# the OperatorTable object on the first message before starting shuffling.
Message OperatorTable := OperatorTable
