/*#io
Collector ioDoc(
docCopyright("Steve Dekorte", 2002)
docLicense("BSD revised")
*/

#ifndef IoCollector_DEFINED
#define IoCollector_DEFINED 1

#include "IoObject.h"

#ifdef __cplusplus
extern "C" {
#endif

IoObject *IoCollector_proto(void *state);

#ifdef __cplusplus
}
#endif
#endif
